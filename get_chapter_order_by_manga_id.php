<?php
require './includes/db.php';
include './model/Response.php';
include './model/Chapter.php';

if(!isset($_POST['mangaid'])){
	$response = 'Not permission';
	$status = 403;
}
else{
$mangaid = $_POST['mangaid'];
$query = "SELECT chapter.chapter_id, chapter.chapter_title, chapter.chapter_thumbnail, chapter.chapter_content, chapter.chapter_created, chapter.chapter_updated, chapter.chapter_views, manga.title FROM `chapter` INNER JOIN `manga` ON chapter.parent_post = manga.id WHERE chapter.parent_post = $mangaid ORDER BY `chapter`.`chapter_created` DESC";
$data = mysqli_query($conn, $query);
$chapterArr = array();

	if (mysqli_num_rows($data) > 0){
		while($row = mysqli_fetch_array($data)){
			array_push($chapterArr, new Chapter(
				$row['chapter_id'],
				$row['chapter_title'],
				$row['chapter_thumbnail'],
				$row['chapter_content'],
				$row['chapter_created'],
				$row['chapter_updated'],
				$row['chapter_views'],
				$row['title']));
		}
		$response = $chapterArr;
		$status = 200;
	}else{
		$response = 'No data';
		$status = 400;
	}

}

$resutArr = array();
array_push($resutArr, new Response($response, $status));
echo json_encode($resutArr);

mysqli_close($conn);
?>