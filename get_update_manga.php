<?php
require './includes/db.php';
include './model/Response.php';
include './model/Category.php';
include './model/Manga.php';

$query = "SELECT * FROM `manga` WHERE 1 ORDER BY `updated_at` DESC";
$data = mysqli_query($conn, $query);
$catArr = array();
$mangaArr = array();

if (mysqli_num_rows($data) > 0){
while($row = mysqli_fetch_array($data)){
			$postId = $row['id'];
			$query = "SELECT category.cat_id, category.cat_name, category.cat_des, category.cat_image, category.created_at,category.updated_at  FROM `post_meta` INNER JOIN `category` ON post_meta.genre_id = category.cat_id WHERE post_id='$postId'";	
			$postMeta = mysqli_query($conn, $query);
			if(mysqli_num_rows($postMeta) > 0){
				while($row1 = mysqli_fetch_array($postMeta)){
				array_push($catArr, new Category(
					$row1['cat_id'],
					$row1['cat_name'],
					$row1['cat_des'],
					$row1['cat_image'],
					$row1['created_at'],
					$row1['updated_at']));
				}
				
			}
		array_push($mangaArr, new Manga(
			$row['id'],
			$row['title'],
			$row['content'],
			$row['thumbnail'],
			$row['cover'],
			$row['artist'],
			$catArr,
			$row['created_at'],
			$row['updated_at'],
			$row['views']));
	}
	$status = 200;
	$response = $mangaArr;

}else{
$response = "No data";
$status = 400;

}

$resutArr = array();
array_push($resutArr, new Response($response, $status));
echo json_encode($resutArr);

mysqli_close($conn);


?>