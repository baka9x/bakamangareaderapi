<?php
require './includes/db.php';
include './model/Response.php';
include './model/Manga.php';


if(!isset($_POST['catid'])){
	$response = 'Not permission';
	$status = 403;

}else{
	$catId = $_POST['catid'];
	$query = "SELECT manga.id, manga.title, manga.content, manga.thumbnail, manga.cover, manga.artist, manga.created_at, manga.updated_at, manga.views, category.cat_name FROM post_meta INNER JOIN category ON post_meta.genre_id = category.cat_id INNER JOIN manga ON post_meta.post_id = manga.id WHERE cat_id = '$catId'";
	$data = mysqli_query($conn, $query);
	$mangaArr = array();
	if (mysqli_num_rows($data) > 0){
	while($row = mysqli_fetch_array($data)){
					array_push($mangaArr, new Manga(
						$row['id'],
						$row['title'],
						$row['content'],
						$row['thumbnail'],
						$row['cover'],
						$row['artist'],
						$row['cat_name'],
						$row['created_at'],
						$row['updated_at'],
						$row['views']));
					
				}
		
		$status = 200;
		$response = $mangaArr;

	}else{
	$response = "No data";
	$status = 400;

	}

}

$resutArr = array();
array_push($resutArr, new Response($response, $status));
echo json_encode($resutArr);

mysqli_close($conn);


?>