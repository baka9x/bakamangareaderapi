<?php

class Category{
	function __construct($cat_id, $name, $des, $image, $created_at, $updated_at){
		$this->cat_id = $cat_id;
		$this->name = $name;
		$this->des = $des;
		$this->image = $image;
		$this->created_at = $created_at;
		$this->updated_at = $updated_at;
	}

}
