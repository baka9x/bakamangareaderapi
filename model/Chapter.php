<?php

class Chapter{
	function __construct($chapter_id, $chapter_title, $chapter_thumbnail, $chapter_content, $chapter_created, $chapter_updated, $chapter_views, $manga_title){
		$this->chapter_id = $chapter_id;
		$this->chapter_title = $chapter_title;
		$this->chapter_thumbnail = $chapter_thumbnail;
		$this->chapter_content = $chapter_content;
		$this->chapter_created = $chapter_created;
		$this->chapter_updated = $chapter_updated;
		$this->chapter_views = $chapter_views;
		$this->manga_title = $manga_title;
	}
}