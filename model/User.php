<?php

class User{
	function __construct($id, $fullname, $username, $avatar, $email, $is_online, $created_at, $type){
		$this->id = $id;
		$this->fullname = $fullname;
		$this->username = $username;
		$this->avatar = $avatar;
		$this->email = $email;
		$this->is_online = $is_online;
		$this->created_at = $created_at;
		$this->type = $type;
	}


}