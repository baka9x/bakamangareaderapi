<?php
require './includes/db.php';
include './model/Response.php';
include './model/Category.php';

if (!isset($_POST['catid'])){
	$response = 'Not permission';
	$status = 403;

}else{
	$catId = $_POST['catid'];
	$query = "SELECT * FROM `category` WHERE cat_id = '$catId'";
	$data = mysqli_query($conn, $query);
	$catArr = array();

	if (mysqli_num_rows($data) > 0){
	while($row = mysqli_fetch_array($data)){
			array_push($catArr, new Category(
				$row['cat_id'],
				$row['cat_name'],
				$row['cat_des'],
				$row['cat_image'],
				$row['created_at'],
				$row['updated_at']));
		}
		$status = 200;
		$response = $catArr;

	}else{
	$response = "No data";
	$status = 400;

	}

}

$resutArr = array();
array_push($resutArr, new Response($response, $status));
echo json_encode($resutArr);

mysqli_close($conn);


?>