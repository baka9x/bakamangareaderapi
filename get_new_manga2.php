<?php
require './includes/db.php';
include './model/Response.php';
include './model/Category.php';
include './model/Manga.php';

$query = "SELECT manga.title, manga.thumbnail, manga.cover, manga.artist, manga.created_at, manga.updated_at, manga.views, category.cat_name FROM ((post_meta INNER JOIN manga ON post_meta.post_id = manga.id) INNER JOIN category ON post_meta.cat_id = category.id) ORDER BY manga.created_at DESC";
$data = mysqli_query($conn, $query);
$catArr = array();
$mangaArr = array();

if (mysqli_num_rows($data) > 0){
while($row = mysqli_fetch_array($data)){
		$categoryId = $row['cat_id'];
		
			$query = "SELECT * FROM `category` WHERE id='$categoryId'";	
			$catData = mysqli_query($conn, $query);
			if(mysqli_num_rows($catData) > 0){
				while($row1 = mysqli_fetch_array($catData)){
				array_push($catArr, new Category(
					$row1['id'],
					$row1['cat_name'],
					$row1['cat_des'],
					$row1['cat_image'],
					$row1['created_at'],
					$row1['updated_at']));
				}
				
			}
		
		array_push($mangaArr, new Manga(
			$row['id'],
			$row['title'],
			$row['content'],
			$row['thumbnail'],
			$row['cover'],
			$row['artist'],
			$catArr,
			$row['created_at'],
			$row['updated_at'],
			$row['views']));
	}
	$status = 200;
	$response = $mangaArr;

}else{
$response = "No data";
$status = 400;

}

$resutArr = array();
array_push($resutArr, new Response($response, $status));
echo json_encode($resutArr);

mysqli_close($conn);


?>